import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AppServiceComponent } from './app.service';
import { BaseComponent } from './base.component';
import { AccountComponent } from './account/account.component';
import { ModulesComponent } from './modules/modules.component';
import { MainComponent } from './main/main.component';
import { AdvertiseComponent } from './advertise/advertise.component';
import { ActivateComponent } from './activate/activate.component';
import { ChangeForgotComponent } from './change-forgot/change-forgot.component';
import { PaymentComponent } from './payment/payment.component';
import { StartPageComponent } from './start-page/start-page.component';
import { ResoucePageComponent } from './resouce-page/resouce-page.component';
import { ArticlePageComponent } from './article-page/article-page.component';
import { PodcastPageComponent } from './podcast-page/podcast-page.component';
import { PartnerAssociateComponent } from './partner-associate/partner-associate.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AdminToolComponent } from './admin-tool/admin-tool.component';
import { ChangeUserAccessComponent } from './change-user-access/change-user-access.component';
import { FileManagerComponent } from './file-manager/file-manager.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    RegisterComponent,
    ForgetPasswordComponent,
    AppServiceComponent,
    BaseComponent,
    AccountComponent,
    ModulesComponent,
    MainComponent,
    AdvertiseComponent,
    ActivateComponent,
    ChangeForgotComponent,
    PaymentComponent,
    StartPageComponent,
    ResoucePageComponent,
    ArticlePageComponent,
    PodcastPageComponent,
    PartnerAssociateComponent,
    AdminToolComponent,
    ChangeUserAccessComponent,
    FileManagerComponent,
  ],
  imports: [
    CKEditorModule,
    DragDropModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule
  ],

  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
