import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  constructor(public base: BaseComponent, private appService: AppServiceComponent) {

  }
  public modules: any[] = [];
  ngOnInit() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('module/getModule', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
        this.base.logOut();
      }
      if (response['success'] === true) {
        this.modules = response['modulesList'];
      } else {
        alert(response['message']);
        this.base.logOut();
      }
    });
  }
  public moduleFunc(pageName: string, hasAccess: boolean) {
    if (hasAccess) {
      this.base.navigateToPage(pageName);
    } else {
      alert('Insufficient access, please upgrate.');
    }

  }
  public navigateToPaymentPage() {
    this.base.navigateToPage('payment');
  }
}
