import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})

export class StartPageComponent implements OnInit {
  public Editor = ClassicEditor;
  public currentSection = 0;
  public page_contents: any[] = [];
  public accesses: any[] = [];
  public selectedType = 0;
  public addItemStatus = false;
  public description = '';
  public showEditNum = 0;
  public showDesEditNum = 0;
  public desChange = {};
  constructor(public base: BaseComponent, private appService: AppServiceComponent, public domSanitizer: DomSanitizer) {
  }

  public addItem(title: string, link: string) {
    link = link.trim();
    if (this.selectedType == 2 && link.length != 11) {
      alert('Bad id. It is 11 digit length and after "v=" in youtube url ');
      return;
    }
    if (!(this.selectedType == 2 || this.selectedType == 1)) {
      alert('Select Type.');
      return;
    }
    title = title.trim();
    if (title.length < 2) {
      alert('Title is too short.');
      return;
    }

    if (this.description.length < 2 && this.selectedType == 1) {
      alert('Description is too short.');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['title'] = title.trim();
    postData['type'] = this.selectedType;
    postData['link'] = link.trim();
    postData['description'] = this.description;
    const checkBox = {};
    this.accesses.forEach(element => {
      checkBox[element['access_num']] = $('#checkbox' + element['access_num']).prop('checked');
    });
    postData['checkBox'] = checkBox;
    const res = this.appService.postClient('start_page/addItem', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables(true);
        this.addItemStatus = false;

      } else {
        alert(response['message']);
      }
    });
  }
  public getUrl(url: string) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngOnInit() {
    this.updateTables(false);

  }
  private updateTables(moveTobButton: boolean) {
    this.addItemStatus = false;
    this.showEditNum = 0;
    this.showDesEditNum = 0;
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('start_page/getPage', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.page_contents = response['start_page'];
        this.getAccess();
        if (moveTobButton) {
          this.base.scrollToButtom();
        }
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
  public getAccess() {
    if (!this.base.canAdmin) { return; }
    if (this.accesses.length > 0) { return; }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('start_page/getAccesses', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.accesses = response['accesses'];

      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }

  public delete(id: number) {
    if (confirm('Are you sure you want to delete?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['id'] = id;
      const res = this.appService.postClient('start_page/delete', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables(false);
          this.addItemStatus = false;
        } else {
          alert(response['message']);
        }
      });
    }

  }
  public hideShowAdd() {
    this.addItemStatus = !this.addItemStatus;
    this.base.scrollToButtom();

  }

  public newOrder(event: CdkDragDrop<string[]>) {
    if (this.base.canAdmin) {
      moveItemInArray(this.page_contents, event.previousIndex, event.currentIndex);
      let index = 1;
      const postArray = [];
      for (const eachItem of this.page_contents) {
        const tempArray = {};
        tempArray['id'] = eachItem['id'];
        tempArray['new_orderid'] = index.toString();
        postArray.push(tempArray);
        index++;
      }
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['new_order'] = postArray;
      const res = this.appService.postClient('start_page/newOrder', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables(false);
        } else {
          alert(response['message']);
        }
      });
    }
  }
  public changeDes({ editor }: ChangeEvent) {
    this.description = editor.getData();
  }
  public saveTopicEdit(id: number) {
    if (!this.base.canAdmin) { return; }
    const inputid = '#editTitleInput' + id.toLocaleString();
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = $(inputid).val().toString().trim();
    const res = this.appService.postClient('start_page/editTitle', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables(false);
      } else {
        alert(response['message']);
      }
    });
  }
  public desChangeEditor({ editor }: ChangeEvent, id: number) {
    this.desChange[id] = editor.getData();
  }
  public saveDesChange(id: number) {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = this.desChange[id];
    const res = this.appService.postClient('start_page/saveDesChange', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables(false);
      } else {
        alert(response['message']);
      }
    });
  }
}
