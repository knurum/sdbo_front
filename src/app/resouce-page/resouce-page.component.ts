import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { saveAs } from 'file-saver/src/FileSaver';
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resouce-page',
  templateUrl: './resouce-page.component.html',
  styleUrls: ['./resouce-page.component.css']
})

export class ResoucePageComponent implements OnInit {

  constructor(
    public base: BaseComponent,
    private appService: AppServiceComponent,
    private activatedRoutes: ActivatedRoute,
    private routers: Router) {
  }
  public Editor = ClassicEditor;
  public source_base_1: any[] = [];
  public source_base_2: any[] = [];
  public source_popup_right_2: any[] = [];
  public source_popup_right_2Count: any[] = [];
  public source_popup_left_1: any[] = [];
  public selectedBase2 = 0;
  public windowDisplay = false;
  public source_popup_right_1: any[] = [];
  public addSource_base_1textAreaName = '';
  public addSource_base_1textAreaDescription = '';
  public addSource_base_1Display = false;
  public addSource_base_2Display = -1;
  public source_base_1IconId = 0;
  public editsource_base_2Box = 0;
  public Selectedsource_popup_right_1 = 0;
  public addArticleAreOpen = false;
  public editDesMode = false;
  public selectedPopUpLeftAddItem = 0;
  public selectedPopUpLeftAddItemBase1 = 0;
  public selectedPopUpLeftAddItemBase2 = 0;
  public forward: number[] = [];
  public backward: number[] = [];
  public editTopicTitleMode = false;
  public edittopictitleNum = 0;
  public editTopicContentMode = false;
  public edittopicContentNum = 0;
  public editAttFileNum = 0;
  public editPodcastNum = 0;
  public addTopicDes = '';
  public saveTopicDes = {};
  public popDes = '';
  public saveBase1Des = {};
  private pdfToUpload: File = null;
  public addAttDes = '';

  ngOnInit() {
    this.updateTablesInitial();
  }
  private assignParam() {
    this.activatedRoutes.queryParams.subscribe(params => {
      if (params['window'] == 'true' && params['base2'] > 0) {

        this.selectedBase2 = params['base2'];
        this.windowDisplay = true;
        if (params['art'] > 0) {
          this.Selectedsource_popup_right_1 = params['art'];
        }
      }

    });
  }
  private updateTablesInitial() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('source/getSource', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.source_base_1 = response['source_base_1'];
        this.source_base_2 = response['source_base_2'];
        this.assignParam();
        this.openWindowUpdate();
        this.opensource_popup_right_1Update();
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
  public addParamS(paramName: string, paramValue: string) {
    this.routers.navigate([], {
      relativeTo: this.activatedRoutes,
      queryParams: {
        [paramName]: paramValue
      },

      queryParamsHandling: 'merge',
      // preserve the existing query params in the route
      skipLocationChange: false,
      // do not trigger navigation
      preserveFragment: true
    });
  }
  public addTwoParamS(paramName: string, paramValue: string, paramName2: string, paramValue2: string) {
    this.routers.navigate([], {
      relativeTo: this.activatedRoutes,
      queryParams: {
        [paramName]: paramValue,
        [paramName2]: paramValue2
      },

      queryParamsHandling: 'merge',
      // preserve the existing query params in the route
      skipLocationChange: false,
      // do not trigger navigation
      preserveFragment: true
    });
  }

  public changesource_base_2(id: number) {
    const inputid = '#source_base_2TitleId' + id.toLocaleString();
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = $(inputid).val();

    const res = this.appService.postClient('source/editSaveBase2', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables();
        this.editsource_base_2Box = 0;
      } else {
        alert(response['message']);
      }
    });
  }

  public deletesource_base_2(id: number) {
    if (confirm('Are you sure you want to delete?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['source_base_2'] = id;
      const res = this.appService.postClient('source/deleteBase2', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables();
        } else {
          alert(response['message']);
          this.base.navigateToPage('modules');
        }
      });
    }
  }
  public editsource_base_2(id: number) {
    if (this.editsource_base_2Box == id) {
      this.editsource_base_2Box = 0;
    } else {
      this.editsource_base_2Box = id;
    }
  }

  public deleteTopic(id: number) {
    if (confirm('Are you sure you want to delete?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['source_base_1'] = id;
      const res = this.appService.postClient('source/deleteBase1', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables();
        } else {
          alert(response['message']);
          this.base.navigateToPage('modules');
        }
      });
    }
  }
  public moreClicksource_base_1(id: number) {
    if (this.source_base_1IconId == id) {
      this.source_base_1IconId = 0;
    } else {
      this.source_base_1IconId = id;
    }

  }
  public showaddSource_base_2(id: number) {
    if (this.addSource_base_2Display === id) {
      this.addSource_base_2Display = -1;
    } else {
      this.addSource_base_2Display = id;
    }
  }
  private updateTables() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('source/getSource', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.source_base_1 = response['source_base_1'];
        this.source_base_2 = response['source_base_2'];
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
  public openWindow(itemId: number) {
    this.initBackwardForward();
    this.selectedBase2 = itemId;
    this.windowDisplay = true;
    this.openWindowUpdate();
    this.addTwoParamS('window', 'true', 'base2', itemId.toLocaleString());

  }
  private openWindowUpdate() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['source_base_2'] = this.selectedBase2;
    const res = this.appService.postClient('source/getPopup', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.source_popup_right_1 = response['source_popup_right_1'];
        this.source_popup_right_2Count = response['source_popup_right_2Count'];
        this.source_popup_left_1 = response['source_popup_left_1'];
      } else {
        alert(response['message']);
      }
    });
  }
  public closeWindow() {
    this.windowDisplay = false;
    this.cancelEdit();
  }
  public getSecondLayer(id: number) {
    const layer_2_child: any[] = [];
    for (const layer_2_each of this.source_base_2) {
      if (layer_2_each['parent_id'] === id) {
        layer_2_child.push(layer_2_each);
      }
    }
    return layer_2_child;
  }
  public saveItemIntopic(id: number) {
    const textAreaid = '#addSource_base_2textAreaid' + id.toLocaleString();
    const value = $(textAreaid).val().toString().trim();
    if (value.length < 1) {
      alert('Entry is empty.');
      return;
    }

    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = value;
    const res = this.appService.postClient('source/addBase2', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        $(textAreaid).val('');
        this.updateTables();
      } else {
        alert(response['message']);
      }
    });
  }
  public saveTopic() {
    this.addSource_base_1textAreaName = this.addSource_base_1textAreaName.trim();
    this.addSource_base_1textAreaDescription = this.addSource_base_1textAreaDescription.trim();
    if (this.addSource_base_1textAreaName.length < 1) {
      alert('Name is emptye');
      return;
    }

    if (this.addSource_base_1textAreaDescription.length < 1) {
      alert('Description is emptye');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['name'] = this.addSource_base_1textAreaName;
    postData['description'] = this.addSource_base_1textAreaDescription;
    const res = this.appService.postClient('source/addBase1', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.addSource_base_1Clear();
        this.updateTables();
      } else {
        alert(response['message']);
      }
    });
  }
  public newOrdersource_base_1(event: CdkDragDrop<string[]>) {
    if (this.base.canEdit) {
      moveItemInArray(this.source_base_1, event.previousIndex, event.currentIndex);
      let index = 1;
      const postArray = [];
      for (const eachItem of this.source_base_1) {
        const tempArray = {};
        tempArray['id'] = eachItem['id'];
        tempArray['new_orderid'] = index.toString();
        postArray.push(tempArray);
        index++;
      }
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['new_order'] = postArray;
      const res = this.appService.postClient('source/orderBase1', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables();
        } else {
          alert(response['message']);
        }
      });
    }
  }
  public newOrdersource_base_2(event: CdkDragDrop<string[]>, id: number) {
    if (this.base.canEdit) {
      const newArray: any[] = this.getSecondLayer(id);
      moveItemInArray(newArray, event.previousIndex, event.currentIndex);
      let index = 1;
      const postArray = [];
      for (const eachItem of newArray) {
        const tempArray = {};
        tempArray['id'] = eachItem['id'];
        tempArray['new_orderid'] = index.toString();
        postArray.push(tempArray);
        index++;
      }
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['new_order'] = postArray;
      const res = this.appService.postClient('source/orderBase2', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.updateTables();
        } else {
          alert(response['message']);
        }
      });
    }
  }
  public getArticleCount(id: number) {
    const count = this.base.selectValueFromPrompid(this.source_popup_right_2Count, 'source_popup_right_1', id, 'count');
    return (count > 0) ? count : 0;
  }
  public addSource_base_1Clear() {
    this.addSource_base_1textAreaName = '';
    this.addSource_base_1textAreaDescription = '';
  }
  public opensource_popup_right_1(id: number) {
    this.addArticleAreOpen = false;
    if (this.getArticleCount(id) == 0 && !this.base.canEdit) {
      return;
    }
    this.Selectedsource_popup_right_1 = id;
    this.opensource_popup_right_1Update();
    this.addParamS('art', this.Selectedsource_popup_right_1.toString());
  }
  private opensource_popup_right_1Update() {

    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['source_popup_right_1'] = this.Selectedsource_popup_right_1;
    postData['source_base_2'] = this.selectedBase2;
    const res = this.appService.postClient('source/openPopRight', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.source_popup_right_2 = response['source_popup_right_2'];
      } else {
        alert(response['message']);
      }
    });
  }
  public newOrdersource_popup_right_2(event: CdkDragDrop<string[]>) {
    this.addArticleAreOpen = false;
    if (!this.base.canEdit) {
      return;
    }
    moveItemInArray(this.source_popup_right_2, event.previousIndex, event.currentIndex);
    let index = 1;
    const postArray = [];
    for (const eachItem of this.source_popup_right_2) {
      const tempArray = {};
      tempArray['id'] = eachItem['id'];
      tempArray['new_orderid'] = index.toString();
      postArray.push(tempArray);
      index++;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['new_order'] = postArray;
    const res = this.appService.postClient('source/orderPopRight2', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables();
      } else {
        alert(response['message']);
      }
    });
  }
  public deletesource_popup_right_2(id: number) {
    this.addArticleAreOpen = false;
    if (confirm('Are you sure you want to delete?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['source_popup_right_2'] = id;
      const res = this.appService.postClient('source/deletePopRight2', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.opensource_popup_right_1Update();
          this.openWindowUpdate();
        } else {
          alert(response['message']);
          this.base.navigateToPage('modules');
        }
      });
    }
  }
  public areaShowSource_popup_right_2() {
    if (this.addArticleAreOpen) {
      this.addArticleAreOpen = false;
    } else {
      this.addArticleAreOpen = true;
    }
  }
  public source_popup_right_2Save(name: string, des: string, link: string) {
    name = name.trim();
    des = des.trim();
    link = link.trim();
    if (name.length < 3) {
      alert('Name should be more descriptive.');
      return;
    }
    if (link.toLocaleLowerCase().substring(0, 4) != 'http' && link.length < 6) {
      alert('Links should start with http');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['name'] = name;
    postData['des'] = des;
    postData['link'] = link;
    postData['source_base_2'] = this.selectedBase2;
    postData['source_popup_right_1'] = this.Selectedsource_popup_right_1;
    const res = this.appService.postClient('source/addPopRight2', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        $('#source_popup_right_2name').val('');
        $('#source_popup_right_2link').val('');
        this.opensource_popup_right_1Update();
        this.openWindowUpdate();
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }

  public savePopUpLeftAddItem() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['source_base_2'] = this.selectedBase2;
    postData['selectedPopUpLeftAddItem'] = this.selectedPopUpLeftAddItem;
    postData['selectedPopUpLeftAddItemBase2'] = this.selectedPopUpLeftAddItemBase2;
    const res = this.appService.postClient('source/addPopleft1', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.selectedPopUpLeftAddItemBase2 = 0;
        this.selectedPopUpLeftAddItemBase1 = 0;
        this.selectedPopUpLeftAddItem = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public edit_pop_left_2(id: number) {
    if (confirm('Are you sure you want to delete?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['id'] = id;
      const res = this.appService.postClient('source/delPopLeft', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.selectedPopUpLeftAddItemBase2 = 0;
          this.selectedPopUpLeftAddItemBase1 = 0;
          this.selectedPopUpLeftAddItem = 0;
          this.openWindowUpdate();
        } else {
          alert(response['message']);
        }
      });
    }
  }
  public newOrderPopLeft(event: CdkDragDrop<string[]>) {
    if (!this.base.canEdit) {
      return;
    }
    moveItemInArray(this.source_popup_left_1, event.previousIndex, event.currentIndex);
    let index = 1;
    const postArray = [];
    for (const eachItem of this.source_popup_left_1) {
      const tempArray = {};
      tempArray['id'] = eachItem['id'];
      tempArray['new_orderid'] = index.toString();
      postArray.push(tempArray);
      index++;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['new_order'] = postArray;
    const res = this.appService.postClient('source/orderPopLeft', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.selectedPopUpLeftAddItemBase2 = 0;
        this.selectedPopUpLeftAddItemBase1 = 0;
        this.selectedPopUpLeftAddItem = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public changePup(id: number) {
    this.backward.push(this.selectedBase2);
    this.selectedBase2 = id;
    this.forward = [];
    this.addParamS('base2', this.selectedBase2.toLocaleString());
    this.openWindowUpdate();
  }
  public backwardFunc() {
    this.forward.push(this.selectedBase2);
    this.selectedBase2 = this.backward[this.backward.length - 1];
    this.addParamS('base2', this.selectedBase2.toLocaleString());
    this.backward.pop();
    this.openWindowUpdate();
  }
  public forwardFunc() {
    this.backward.push(this.selectedBase2);
    this.selectedBase2 = this.forward[this.forward.length - 1];
    this.addParamS('base2', this.selectedBase2.toLocaleString());
    this.forward.pop();
    this.openWindowUpdate();
  }
  private initBackwardForward() {
    this.forward = [];
    this.backward = [];
  }
  public savePopUpLeftAddLink(name: string, link: string) {
    name = name.trim();
    link = link.trim();
    if (!(name.length > 1)) {
      alert('Name too short');
      return;
    }
    if (!(link.length > 1)) {
      alert('Link too short');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['source_base_2'] = this.selectedBase2;
    postData['selectedPopUpLeftAddItem'] = this.selectedPopUpLeftAddItem;
    postData['name'] = name;
    postData['link'] = link.replace(/\r?\n/g, '<br>');
    const res = this.appService.postClient('source/addPopleft1', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.selectedPopUpLeftAddItemBase2 = 0;
        this.selectedPopUpLeftAddItemBase1 = 0;
        this.selectedPopUpLeftAddItem = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public savePopUpLeftTopicTitle(id: number) {
    const postData = {};
    const textAreaid = '#rightTDTitleAddTopic' + id.toLocaleString();
    const value = $(textAreaid).val().toString().trim();
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = value;
    const res = this.appService.postClient('source/saveTopicTitle', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.editTopicTitleMode = false;
        this.edittopictitleNum = 0;
        this.editTopicContentMode = false;
        this.edittopicContentNum = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }

  public popUpAddtopicDes({ editor }: ChangeEvent) {
    this.addTopicDes = editor.getData();
  }
  public savePopUpLeftAddTopic(name: string) {
    name = name.trim();
    if (!(name.length > 1)) {
      alert('Name too short');
      return;
    }
    if (!(this.addTopicDes.length > 1)) {
      alert('Description too short');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['source_base_2'] = this.selectedBase2;
    postData['selectedPopUpLeftAddItem'] = this.selectedPopUpLeftAddItem;
    postData['name'] = name;
    postData['link'] = this.addTopicDes;
    const res = this.appService.postClient('source/addPopleft1', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.selectedPopUpLeftAddItemBase2 = 0;
        this.selectedPopUpLeftAddItemBase1 = 0;
        this.selectedPopUpLeftAddItem = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public popUpSaveTopicDes({ editor }: ChangeEvent, id: number) {
    this.saveTopicDes[id] = editor.getData();
  }
  public savePopUpLeftTopicDes(id: number) {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['value'] = this.saveTopicDes[id];
    const res = this.appService.postClient('source/saveTopicDes', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.editTopicTitleMode = false;
        this.edittopictitleNum = 0;
        this.editTopicContentMode = false;
        this.edittopicContentNum = 0;
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public changeDescirption({ editor }: ChangeEvent) {
    this.popDes = editor.getData();
  }
  public saveDescirption() {

    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['value'] = this.popDes;
    postData['source_base_2'] = this.selectedBase2;
    const res = this.appService.postClient('source/savePopDes', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.updateTables();
        this.editDesMode = false;
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
  public addBase1Des({ editor }: ChangeEvent) {
    this.addSource_base_1textAreaDescription = editor.getData();
  }

  public changeBase1Des({ editor }: ChangeEvent, id: number) {
    this.saveBase1Des[id] = editor.getData();
  }
  public changesource_base_1(id: number) {
    const inputid = '#source_base_1TitleId' + id.toLocaleString();
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    postData['name'] = $(inputid).val();
    postData['description'] = this.saveBase1Des[id];
    const res = this.appService.postClient('source/editSaveBase1', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.source_base_1IconId = 0;
        this.updateTables();
      } else {
        alert(response['message']);
      }
    });
  }
  public handleFileInput(files: FileList) {
    this.pdfToUpload = files.item(0);
    console.log(this.pdfToUpload);
  }
  public submitpdfUpload(title: string) {
    console.log(title);
    const formData: FormData = new FormData();
    formData.append('access_token', this.base.accessToken);
    formData.append('source_base_2', this.selectedBase2.toLocaleString());
    formData.append('fileName', this.pdfToUpload.name);
    formData.append('title', title);
    formData.append('description', this.addAttDes);
    formData.append('fileToUpload', this.pdfToUpload);
    const res = this.appService.postFileUpload('source/uploadAttachment', formData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.cancelEdit();
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }
  public popUpAddAttDes({ editor }: ChangeEvent) {
    this.addAttDes = editor.getData();
  }
  public cancelEdit() {
    this.addSource_base_1textAreaName = '';
    this.addSource_base_1textAreaDescription = '';
    this.addSource_base_1Display = false;
    this.addSource_base_2Display = -1;
    this.source_base_1IconId = 0;
    this.editsource_base_2Box = 0;
    this.Selectedsource_popup_right_1 = 0;
    this.addArticleAreOpen = false;
    this.editDesMode = false;
    this.selectedPopUpLeftAddItem = 0;
    this.selectedPopUpLeftAddItemBase1 = 0;
    this.selectedPopUpLeftAddItemBase2 = 0;
    this.editTopicTitleMode = false;
    this.edittopictitleNum = 0;
    this.editTopicContentMode = false;
    this.edittopicContentNum = 0;
    this.addTopicDes = '';
    this.saveTopicDes = {};
    this.popDes = '';
    this.saveBase1Des = {};
    this.pdfToUpload = null;
    this.addAttDes = '';
    this.editAttFileNum = 0;
    this.editPodcastNum = 0;
    this.addTwoParamS('window', 'false', 'art', '0');
  }
  public downLoadAtt(id: number, name: string, ext: string): void {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['id'] = id;
    this.appService.getFile('source/downLoadAtt', postData)
      .subscribe(
        (val: Blob) => {
          this.createImageFromBlob(val, name, ext);
        },
        response => {
          console.log('POST in error', response);
        },
        () => {
          console.log('POST observable is now completed.');
        });
  }
  private createImageFromBlob(blobFile: Blob, name: string, ext: string) {
    name = name.replace(/[^a-zA-Z0-9]/g, '_');
    const now = new Date();
    const dateTime = now.getFullYear() + '_' + (now.getMonth() + 1) + '_' + now.getDate() + '_'
      + now.getHours() + '_' + now.getMinutes() + '_' + now.getSeconds() + '_' + now.getMilliseconds();
    let fileName = name + '_' + dateTime + '.' + ext;
    while (fileName.includes('__')) {
      fileName = fileName.replace('__', '_');
    }
    if (blobFile) {
      const file = new File([blobFile], 'tempFile');
      saveAs(file, fileName);
    }
  }
  public changeUploadFile(id: number) {
    console.log(id);
    const formData: FormData = new FormData();
    formData.append('access_token', this.base.accessToken);
    formData.append('id', id.toString());
    formData.append('fileName', this.pdfToUpload.name);
    formData.append('fileToUpload', this.pdfToUpload);
    const res = this.appService.postFileUpload('source/changeAttachment', formData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.cancelEdit();
        this.openWindowUpdate();
      } else {
        alert(response['message']);
      }
    });
  }

  public source_popup_right_2Edit(id: number) {
    const name = $('#source_popup_right_2name' + id.toLocaleString()).val().toString().trim();
    const des = $('#source_popup_right_2des' + id.toLocaleString()).val().toString().trim();
    const link = $('#source_popup_right_2link' + id.toLocaleString()).val().toString().trim();

    if (name.length < 3) {
      alert('Name should be more descriptive.');
      return;
    }
    if (link.toLocaleLowerCase().substring(0, 4) != 'http' && link.length < 6) {
      alert('Links should start with http');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['name'] = name;
    postData['des'] = des;
    postData['link'] = link;
    postData['id'] = id;
    const res = this.appService.postClient('source/editPopRight2', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.editPodcastNum = 0;
        this.opensource_popup_right_1Update();
        this.openWindowUpdate();
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
}
