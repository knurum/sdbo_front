import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';
@Component({
  selector: 'app-podcast-page',
  templateUrl: './podcast-page.component.html',
  styleUrls: ['./podcast-page.component.css'],
})
export class PodcastPageComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    public base: BaseComponent,
    private appService: AppServiceComponent,
    private router: Router) {
  }
  public currentLayer = 1;
  public source_base_1: any[] = [];
  public source_base_2: any[] = [];
  public podcast: any[] = [];
  public allRes: any[] = [];
  public selectedBase1 = 0;
  public selectedBase2 = 0;
  public selectedPodcast = 0;
  public regularMode = true;

  ngOnInit() {

    this.updateTables();

  }
  public addParam(paramName: string, paramValue: string) {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        [paramName]: paramValue
      },

      queryParamsHandling: 'merge',
      // preserve the existing query params in the route
      skipLocationChange: false,
      // do not trigger navigation
      preserveFragment: true
    });
  }

  public addTwoParam(paramName: string, paramValue: string, paramName2: string, paramValue2: string) {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        [paramName]: paramValue,
        [paramName2]: paramValue2
      },

      queryParamsHandling: 'merge',
      // preserve the existing query params in the route
      skipLocationChange: false,
      // do not trigger navigation
      preserveFragment: true
    });
  }
  private assignParam() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['mode'] == 'false') {
        this.regularMode = false;
      }
      if (params['layer'] > 0) {
        this.currentLayer = params['layer'];
      }
      if (params['base1'] > 0) {
        this.selectedBase1 = params['base1'];
        this.assignBase2(params['base1']);
      }
      if (params['base2'] > 0) {
        this.selectedBase2 = params['base2'];
        this.assignPodCast(params['base2']);
      }
      if (params['podcast'] > 0) {
        this.selectedPodcast = params['podcast'];
      }
    });
  }
  public changeMode() {
    this.regularMode = !this.regularMode;
    this.addParam('mode', this.regularMode.toString());
  }

  private updateTables() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('podcast/getPodcast', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.allRes = response;
        this.source_base_1 = response['source_base_1'];
        this.source_base_2 = response['source_base_2'];
        this.podcast = response['podcast'];
        this.assignParam();
      } else {
        alert(response['message']);
      }
    });
  }
  public clickBase1(id: number) {
    this.assignBase2(id);
    this.currentLayer++;
    this.addTwoParam('base1', id.toString(), 'layer', this.currentLayer.toString());

  }
  private assignBase2(id: number) {
    this.source_base_2 = [];
    for (const source_base_2 of this.allRes['source_base_2']) {
      if (source_base_2['parent_id'] == id) {
        this.source_base_2.push(source_base_2);
      }
    }
    this.selectedBase1 = id;
  }
  public clickBase2(id: number) {
    this.assignPodCast(id);
    this.currentLayer++;
    this.addTwoParam('base2', id.toString(), 'layer', this.currentLayer.toString());
  }
  private assignPodCast(id: number) {
    this.podcast = [];
    for (const podcast of this.allRes['podcast']) {
      if (podcast['source_base_2'] == id) {
        this.podcast.push(podcast);
      }
    }
    this.selectedBase2 = id;
  }
}
