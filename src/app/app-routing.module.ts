import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { RegisterComponent } from './register/register.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AccountComponent } from './account/account.component';
import { ModulesComponent } from './modules/modules.component';
import { MainComponent } from './main/main.component';
import { AdvertiseComponent } from './advertise/advertise.component';
import { AppComponent } from '../app/app.component';
import { ActivateComponent } from './activate/activate.component';
import { ChangeForgotComponent } from './change-forgot/change-forgot.component';
import { PaymentComponent } from './payment/payment.component';
import { StartPageComponent } from './start-page/start-page.component';
import { ResoucePageComponent } from './resouce-page/resouce-page.component';
import { ArticlePageComponent } from './article-page/article-page.component';
import { PodcastPageComponent } from './podcast-page/podcast-page.component';
import { PartnerAssociateComponent } from './partner-associate/partner-associate.component';
import { AdminToolComponent } from './admin-tool/admin-tool.component';
import { ChangeUserAccessComponent } from './change-user-access/change-user-access.component';
import { FileManagerComponent } from './file-manager/file-manager.component';

const routes: Routes = [
  { path: 'file-manager', component: FileManagerComponent },
  { path: 'change-user-access', component: ChangeUserAccessComponent },
  { path: 'admin-tool', component: AdminToolComponent },
  { path: 'start_page', component: StartPageComponent },
  { path: 'resouce_page', component: ResoucePageComponent },
  { path: 'article_page', component: ArticlePageComponent },
  { path: 'podcast_page', component: PodcastPageComponent },
  { path: 'partner_associate', component: PartnerAssociateComponent },
  { path: 'change-forgot', component: ChangeForgotComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'app-main', component: AppComponent },
  { path: 'activate', component: ActivateComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'advertise', component: AdvertiseComponent },
  { path: 'modules', component: ModulesComponent },
  { path: 'main', component: MainComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'account', component: AccountComponent },
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [AppComponent, SignInComponent, RegisterComponent,
  AccountComponent, ModulesComponent, MainComponent, AdvertiseComponent, ActivateComponent,
  ChangeForgotComponent, PaymentComponent, AdminToolComponent, ChangeUserAccessComponent, FileManagerComponent];
