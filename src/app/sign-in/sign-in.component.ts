import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent implements OnInit {
  public emailAlert = '';
  public passwordAlert = '';
  public signInValidation = '';
  constructor(public base: BaseComponent, private appService: AppServiceComponent) {
  }
  submitSignIn(emailAddress: string, password: string) {

    emailAddress = emailAddress.trim().toLowerCase();
    if (!this.emailChange(emailAddress)) {
      this.signInValidation = 'Invalid Email.';
      return;
    }

    if (!this.passwordChange(password)) {
      this.signInValidation = 'At least 6 character.';
      return;
    }
    const postData = {};
    postData['emailAddress'] = emailAddress;
    postData['password'] = password;
    const res = this.appService.postClient('sign/signin', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] === true) {
        this.signInValidation = 'Successful.';
        this.assignLogIn(response['access_token'], emailAddress, response['access']);
      } else {
        if (response['message'] !== null) {
          this.signInValidation = response['message'];
        } else {
          this.signInValidation = 'Log in failed.';
        }
      }
    });
  }
  private assignLogIn(access_token: string, emailAddress: string, access: number) {

    this.base.accessToken = access_token;
    this.base.assignAccess(access);
    this.base.emailAddress = emailAddress;
    localStorage.setItem('accessToken', this.base.accessToken);
    this.base.signedIn = true;
    setTimeout(() => {
      this.base.navigateToPage('modules');
    },
      2000);
  }

  ngOnInit() {
    if (this.base.signedIn) {
      this.base.navigateToPage('modules');
    }
  }
  public passwordChange(value: string): boolean {
    if (value.length < 6) {
      this.passwordAlert = 'At least 6 character required.';
      return false;
    } else {
      this.passwordAlert = '';
      return true;
    }
  }
  public emailChange(emailAddress: string): boolean {
    emailAddress = emailAddress.trim().toLowerCase();
    if (!this.base.isValidMailFormat(emailAddress) || emailAddress.length < 4) {
      this.emailAlert = 'Invalid Email.';
      return false;
    } else {
      this.emailAlert = '';
      return true;
    }
  }
}
