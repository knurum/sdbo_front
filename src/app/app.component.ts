import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from './base.component';
import { AppServiceComponent } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  appTitle = 'Home';
  constructor(private cdr: ChangeDetectorRef, public router: Router, public base: BaseComponent, private appService: AppServiceComponent) {
    if (localStorage.getItem('accessToken') != null && localStorage.getItem('accessToken').length > 10) {
      this.base.signedIn = true;
      this.base.accessToken = localStorage.getItem('accessToken');

      const postData = {};
      postData['access_token'] = this.base.accessToken;
      const res = this.appService.postClient('sign/checkToken', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
          this.base.assignAccess(response['access']);
        } else {
          alert(response['message']);
        }
      });
    } else {
      this.base.signedIn = false;
    }
  }
  ngOnInit() {

  }

}
