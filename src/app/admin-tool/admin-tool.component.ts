import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-admin-tool',
  templateUrl: './admin-tool.component.html',
  styleUrls: ['./admin-tool.component.css']
})
export class AdminToolComponent implements OnInit {

    constructor(public base: BaseComponent, private appService: AppServiceComponent) {

  }

  ngOnInit() {
  }

}
