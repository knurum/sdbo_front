import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(public base: BaseComponent, private appService: AppServiceComponent) {

  }

  public layer_1: any[] = [];
  public layer_2: any[] = [];
  public windowDisplay = false;
  public windowName = '';

  ngOnInit() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['module'] = this.base.module;
    const res = this.appService.postClient('data/getMain', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.assignManData(response);
      } else {
        alert(response['message']);
        this.base.logOut();
      }
    });
  }
  public assignManData(mainFrameData: any[]) {
    this.layer_1 = mainFrameData['layer_1'];
    this.layer_2 = mainFrameData['layer_2'];
  }
  public openWindow(itemId: number, windowName: string) {
    this.windowName = windowName;
    this.windowDisplay = true;

  }
  public closeWindow()
  {
this.windowDisplay = false;
  }
  public getSecondLayer(id: number) {
    const layer_2_child: any[] = [];
    for (const layer_2_each of this.layer_2) {
      if (layer_2_each['parent_id'] === id) {
        layer_2_child.push(layer_2_each);
      }
    }
    return layer_2_child;
  }
}
