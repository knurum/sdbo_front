import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.css']
})
export class FileManagerComponent implements OnInit {

  constructor(public base: BaseComponent, private appService: AppServiceComponent) {
  }
  fileToUpload: File = null;
  ngOnInit() {
  }

  public handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }
  public submitUpload() {
    const formData: FormData = new FormData();
    formData.append('access_token', this.base.accessToken);
    formData.append('fileName', this.fileToUpload.name);
    formData.append('description', 'description');
    formData.append('fileToUpload', this.fileToUpload);
    const res = this.appService.postFileUpload('fileManager/fileUpload', formData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {

      } else {
        alert(response['message']);
      }
    });
  }
}
