import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.css']
})
export class ArticlePageComponent implements OnInit {
public currentSection = 0;
  constructor() { }

  ngOnInit() {
  }
  public changeSection(sectionNum: number) {
    if (this.currentSection == sectionNum) {
      this.currentSection = 0;
    } else {
      this.currentSection = sectionNum;
    }
  }
}
