import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  constructor(public base: BaseComponent, private appService: AppServiceComponent) {
  }

  public emailAlert = '';
  public submitValidation = '';
  public items = '';

  ngOnInit() {
  }

  public submitForgotPass(emailAddress: string) {
    emailAddress = emailAddress.trim().toLowerCase();
    if (!this.inputChange(emailAddress)) {
      this.submitValidation = 'Invalid Email.';
      // return;
    }

    this.submitValidation = 'Sending...';
    const postData = {};
    postData['emailAddress'] = emailAddress;
    const res = this.appService.postClient('sign/forgotEmail', postData);
    res.subscribe((response: {}) => {
      if (response['success'] === true) {
        this.submitValidation = 'Successful. Check your email for change password link.';

      } else {
        if (response['message'] != null) {
          this.submitValidation = response['message'];
        } else {
          this.submitValidation = 'Could not send email.';
        }
      }
    });
  }



  public inputChange(emailAddress: string) {
    emailAddress = emailAddress.trim().toLowerCase();
    if (emailAddress === '') {
      this.emailAlert = 'Email Required.';
       return false;
    } else if (this.base.isValidMailFormat(emailAddress)) {
      this.emailAlert = '';
      return true;
    } else {
      this.emailAlert = 'Invalid Email';
      return false;
    }
  }
}
