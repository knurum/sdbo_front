import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUserAccessComponent } from './change-user-access.component';

describe('ChangeUserAccessComponent', () => {
  let component: ChangeUserAccessComponent;
  let fixture: ComponentFixture<ChangeUserAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeUserAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
