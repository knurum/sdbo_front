import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-change-user-access',
  templateUrl: './change-user-access.component.html',
  styleUrls: ['./change-user-access.component.css']
})
export class ChangeUserAccessComponent implements OnInit {

  constructor(public base: BaseComponent, private appService: AppServiceComponent) {

  }
  public users: any[] = [];
  public accesses: any[] = [];
  public schools: any[] = [];
  public orderNum = 0;
  public totalUser = 0;
  public advancedSearch = false;
  public where = '';
  ngOnInit() {
    this.updateTables();
  }
  public doMainSearch(searchValue: string) {
    searchValue = searchValue.trim();
    if (!(searchValue.length > 0)) {
      alert('Search Value is empty.');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['searchValue'] = searchValue;
    console.log(postData);
    const res = this.appService.postClient('admin/doMainSearch', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.orderNum = 0;
        this.users = response['users'];
        this.totalUser = response['totalUser'];
        this.where = response['where'];
      } else {
        alert(response['message']);
      }
    });
  }
  public doAdvancedSearch(minId: number, maxId: number, access: number,
    email: string, fName: string, lName: string, activated: string, school: number, stime: string, etime: string) {

    if (!(minId > 0) && !(maxId > 0) && !(access >= 0) && !(email.trim().length > 0)
      && !(fName.trim().length > 0) && !(lName.trim().length > 0) && activated.trim() == 'noSelection'
      && !(school > 0) && !(stime.trim().length > 0) && !(etime.trim().length > 0)) {
      alert('Search Value is empty.');
      return;
    }

    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['minId'] = minId;
    postData['maxId'] = maxId;
    postData['access'] = access;
    postData['email'] = email.trim();
    postData['fName'] = fName.trim();
    postData['lName'] = lName.trim();
    postData['activated'] = activated.trim();
    postData['school'] = school;
    postData['stime'] = stime.trim();
    postData['etime'] = etime.trim();

    console.log(postData);
    const res = this.appService.postClient('admin/doAdvancedSearch', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.orderNum = 0;
        this.users = response['users'];
        this.totalUser = response['totalUser'];
        this.where = response['where'];
      } else {
        alert(response['message']);
      }
    });
  }
  public changeOrder(id: string, orderNum: number) {

    let keyIsInt = false;
    if (id == 'id' || id == 'school') {
      keyIsInt = true;
    }
    if (orderNum == this.orderNum) {
      this.orderNum = orderNum + 1;
      this.users = this.users.sort(this.base.compareValues(id, 'desc', keyIsInt));
    } else {
      this.orderNum = orderNum;
      this.users = this.users.sort(this.base.compareValues(id, 'asc', keyIsInt));
    }

  }
  private updateTables() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('admin/getUser', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.users = response['users'];
        this.accesses = response['accesses'];
        this.schools = response['schools'];
        this.totalUser = response['totalUser'];
        this.where = '';
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }

  public changeAccess(afterAccess: number, userId: number, email: string, originalAccess: number, element: HTMLSelectElement) {
    if (confirm('Change user ' + email + ' from ' + this.base.selectValueFromPrompid(this.accesses, 'access_num', originalAccess, 'name')
      + '(' + originalAccess + ')' + ' to ' + this.base.selectValueFromPrompid(this.accesses, 'access_num', afterAccess, 'name') +
      '(' + afterAccess + ')?')) {
      const postData = {};
      postData['access_token'] = this.base.accessToken;
      postData['afterAccess'] = afterAccess;
      postData['userId'] = userId;
      const res = this.appService.postClient('admin/changeAccess', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] == null) {
          alert('access incontered issue. Please log again.');
        }
        if (response['success'] === true) {
        } else {
          alert(response['message']);
        }
      });
    } else {
      element.selectedIndex = this.base.getItemIndexFromPrompt(this.accesses, 'access_num', originalAccess);
    }
  }
  public getMoreUser() {
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    postData['minId'] = this.base.findMinFromRecord(this.users, 'id');
    postData['where'] = this.where;
    const res = this.appService.postClient('admin/getMoreUser', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        alert('access incontered issue. Please log again.');
      }
      if (response['success'] === true) {
        this.orderNum = 0;
        this.users = this.users.concat(response['users']);
      } else {
        alert(response['message']);
        this.base.navigateToPage('modules');
      }
    });
  }
}
