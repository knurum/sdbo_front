import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServiceComponent } from './../app.service';
import { BaseComponent } from './../base.component';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {

  public information = '';
  constructor(private activatedRoute: ActivatedRoute, private appService: AppServiceComponent, public base: BaseComponent) {

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const activation_token = params['token'];
      this.information = activation_token;
      if (activation_token == null || activation_token.length < 10) {
        this.information = 'Invalid token.';
        return;
      }
      const postData = {};
      postData['activation_token'] = activation_token;
      const res = this.appService.postClient('sign/activate', postData);
      res.subscribe((response: any[]) => {
         if (response['success'] === true) {
        this.information = 'Successfully activated.';
      } else {
        if (response['message'] !== null) {
          this.information = response['message'];
        } else {
          this.information = 'Activation failed.';
        }
      }
      });
    });
  }
}
