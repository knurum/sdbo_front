import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-root',
    template: ''
})
@Injectable({
    providedIn: 'root'
})
export class AppServiceComponent {

    // public hostUrl = 'http://localhost:4200';
    public hostUrl = 'http://anurum.co.nf';
    constructor(private http: HttpClient) {

        const url = window.location.href;
        const index = url.indexOf('/#');

        if (index > 3) {
            this.hostUrl = url.substring(0, url.indexOf('/#'));
        } else {
            const lastletter = url.substring(url.length - 1, url.length);
            if (lastletter == '/') {
                this.hostUrl = url.substring(0, url.length - 1);
            } else {
                this.hostUrl = url;
            }


        }
    }
    getClient(urlChild: string) {
        return this.http.get(this.getServiceUrl(urlChild));
    }
    postClient(urlChild: string, postData: {}) {
        const postJson = JSON.stringify(postData);
        return this.http.post(this.getServiceUrl(urlChild), postJson);

    }
    public postFileUpload(urlChild: string, formData: FormData) {

        return this.http.post(this.getServiceUrl(urlChild), formData);
    }
    private getServiceUrl(urlChild: string): string {
        let baseURL = 'http://localhost/sdbo_back/';
        if (this.hostUrl.includes('anurum')) {
            baseURL = 'http://anurum.co.nf/api/';
        }

        return baseURL + urlChild + '.php';
    }
    public getFile(urlChild: string, postData: {}) {
        const postJson = JSON.stringify(postData);
        const headers = new HttpHeaders({
        });
        return this.http.post<Blob>(this.getServiceUrl(urlChild), postJson, { headers: headers, responseType: 'blob' as 'json' });
    }
}
