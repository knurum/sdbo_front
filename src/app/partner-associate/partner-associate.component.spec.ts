import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerAssociateComponent } from './partner-associate.component';

describe('PartnerAssociateComponent', () => {
  let component: PartnerAssociateComponent;
  let fixture: ComponentFixture<PartnerAssociateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerAssociateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerAssociateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
