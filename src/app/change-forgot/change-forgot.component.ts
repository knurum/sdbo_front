import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServiceComponent } from './../app.service';
import { BaseComponent } from './../base.component';

@Component({
  selector: 'app-change-forgot',
  templateUrl: './change-forgot.component.html',
  styleUrls: ['./change-forgot.component.css']
})
export class ChangeForgotComponent implements OnInit {

  public information = '';
  public newPass = '';
  public confirm = '';
  public changePassValidation = '';
  public changePassToken = '';
  public pass_value = '';
  public confirm_value = '';
  constructor(private activatedRoute: ActivatedRoute, private appService: AppServiceComponent, public base: BaseComponent) {

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.changePassToken = params['token'];
      if (this.changePassToken == null || this.changePassToken.length < 10) {
        this.information = 'Invalid token.';
        return;
      }
      const postData = {};
      postData['change_pass_token'] = this.changePassToken;
      const res = this.appService.postClient('sign/checkChangePassToken', postData);
      res.subscribe((response: any[]) => {
        if (response['success'] === true) {
          this.information = '';
        } else {
          if (response['message'] !== null) {
            this.information = response['message'];
          } else {
            this.information = 'Wrong token.';
          }
        }
      });
    });
  }
  public submitChangePass(newPass: string, confirmPass: string) {
    if (!this.newPasswordChange(newPass)) {
      this.changePassValidation = 'Invalid password.';
      return;
    }
    if (!this.confirmChange(newPass, confirmPass)) {
      this.changePassValidation = 'Two password does not match.';
      return;
    }
    if (this.changePassToken == null || this.changePassToken.length < 10) {
      this.changePassValidation = 'Invalid token.';
      return;
    }
    this.changePassValidation = 'Sending';
    const postData = {};
    postData['change_pass_token'] = this.changePassToken;
    postData['newpass'] = newPass;
    const res = this.appService.postClient('sign/forgotChangePass', postData);
    res.subscribe((response: {}) => {

      if (response['success'] === true) {
        this.changePassValidation = 'Successfully changed..';

        this.pass_value = '';
        this.confirm_value = '';
      } else {
        if (response['message'] != null) {
          this.changePassValidation = response['message'];
        } else {
          this.changePassValidation = 'Could not change';
        }
      }

    });
  }
  public newPasswordChange(value: string) {
    if (!this.base.isValidPassFormat(value)) {
      this.newPass = 'invalid password.';
      return false;
    } else {
      this.newPass = '';
      return true;
    }
  }
  public confirmChange(newPass: string, confirmPass: string) {
    if (newPass === confirmPass) {
      this.confirm = '';
      return true;
    } else {
      this.confirm = 'Confirm password does not match.';
      return false;
    }
  }
}
