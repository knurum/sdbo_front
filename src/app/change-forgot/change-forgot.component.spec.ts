import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeForgotComponent } from './change-forgot.component';

describe('ChangeForgotComponent', () => {
  let component: ChangeForgotComponent;
  let fixture: ComponentFixture<ChangeForgotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeForgotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeForgotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
