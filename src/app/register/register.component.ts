import { Component, OnInit } from '@angular/core';
import { AppComponent } from './../app.component';
import { AppServiceComponent } from './../app.service';
import { BaseComponent } from './../base.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public schools: any[] = [];
  public first_name = '';
  public last_name = '';
  public zip_code = '';
  public emailAlert = '';
  public schoolAlert = '';
  public registerValidation = '';
  public registrationComplete = false;
  public newPass = '';
  public confirm = '';

  public first_name_value = '';
  public last_name_value = '';
  public email_value = '';
  public pass_value = '';
  public confirm_value = '';


  constructor(private appService: AppServiceComponent, private appComponent: AppComponent, private base: BaseComponent) { }

  ngOnInit() {
    if (this.base.signedIn) {
      this.base.navigateToPage('../modules');
    } else {
      const res = this.appService.getClient('sign/getSchools');
      res.subscribe((response: {}) => {

        if (response['success'] === true) {
          this.schools = response['schools'];
        } else {
          if (response['message'] != null) {
            this.registerValidation = response['message'];
          } else {
            this.registerValidation = 'Could not register';
          }
        }

      });
    }

  }
  public submitRegister(firstName: string, lastName: string, email: string, newpass: string, confirmPass: string, school: number) {
    firstName = firstName.trim();
    lastName = lastName.trim();
    email = email.trim().toLowerCase();
    if (!this.firstNameChange(firstName)) {
      this.registerValidation = 'Invalid First Name.';
      return;
    }
    if (!this.lastNameChange(lastName)) {
      this.registerValidation = 'Invalid Last Name.';
      return;
    }
    if (!this.schoolChange(school)) {
      this.registerValidation = 'Invalid School.';
      return;
    }
    if (!this.emailChange(email)) {
      this.registerValidation = 'Invalid Email.';
      return;
    }
    if (!this.newPasswordChange(newpass)) {
      this.registerValidation = 'Invalid password.';
      return;
    }
    if (!this.confirmChange(newpass, confirmPass)) {
      this.registerValidation = 'Two password does not match.';
      return;
    }

    this.registerValidation = 'Sending';
    const postData = {};
    postData['firstName'] = firstName;
    postData['lastName'] = lastName;
    postData['email'] = email;
    postData['newpass'] = newpass;
    postData['confirmPass'] = confirmPass;
    postData['school'] = school;
    const res = this.appService.postClient('sign/register', postData);
    res.subscribe((response: {}) => {

      if (response['success'] === true) {
        this.registerValidation = 'Successful. Activation link send to your email.';
        this.first_name_value = '';
        this.last_name_value = '';
        this.email_value = '';
        this.pass_value = '';
        this.confirm_value = '';
      } else {
        if (response['message'] != null) {
          this.registerValidation = response['message'];
        } else {
          this.registerValidation = 'Could not register';
        }
      }

    });


  }
  public schoolChange(value: number) {
    if (value == 0) {
      this.schoolAlert = 'Please select your school.';
      return false;
    } else {
      this.schoolAlert = '';
      return true;
    }
  }
  public firstNameChange(value: string) {
    if (!this.base.isValidNameFormat(value)) {
      this.first_name = 'Text Only.';
      return false;
    } else {
      this.first_name = '';
      return true;
    }
  }
  public lastNameChange(value: string) {
    if (!this.base.isValidNameFormat(value)) {
      this.last_name = 'Text Only.';
      return false;
    } else {
      this.last_name = '';
      return true;
    }
  }

  public emailChange(value: string) {
    if (!this.base.isValidMailFormat(value)) {
      this.emailAlert = 'Invalid Email.';
      return false;
    } else {
      this.emailAlert = '';
      return true;
    }
  }
  public newPasswordChange(value: string) {
    if (!this.base.isValidPassFormat(value)) {
      this.newPass = 'invalid password.';
      return false;
    } else {
      this.newPass = '';
      return true;
    }
  }
  public confirmChange(newPass: string, confirmPass: string) {
    if (newPass === confirmPass) {
      this.confirm = '';
      return true;
    } else {
      this.confirm = 'Confirm password does not match.';
      return false;
    }

  }
}
