import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './../base.component';
import { AppServiceComponent } from './../app.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  constructor(public base: BaseComponent, private appService: AppServiceComponent) {
  }
  public modules: any[] = [];
  public accessType: any[] = [];
  ngOnInit() {
    if (!(this.base.signedIn)) {
      this.base.navigateToPage('sign-in');
      return;
    }
    const postData = {};
    postData['access_token'] = this.base.accessToken;
    const res = this.appService.postClient('module/getPaymentOptions', postData);
    res.subscribe((response: any[]) => {
      if (response['success'] == null) {
        this.base.logOut();
      }
      if (response['success'] === true) {
        this.modules = response['modulesList'];
        this.accessType = response['access_type'];
      } else {
        alert(response['message']);
        this.base.logOut();
      }
    });
  }
  public upgrade(accessNum: number) {
    alert('Upgrade to ' + accessNum + '. will add funtion.');

  }
  public downgrade(accessNum: number) {
    alert('Downgrade to ' + accessNum + '. will add funtion.');

  }
}
