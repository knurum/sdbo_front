import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppServiceComponent } from './app.service';


@Component({
    selector: 'app-root',
    template: ''
})
@Injectable({
    providedIn: 'root'
})
export class BaseComponent {
    public signedIn: boolean;
    public emailAddress = '';
    public accessToken: string;
    public module = 0;
    public access = 0;
    public adminAccess = 9;
    public editorAccess = 8;
    public canEdit = false;
    public canAdmin = false;

    constructor(public router: Router,
        private appService: AppServiceComponent) {
        this.signedIn = false;
    }

    public assignAccess(access: number) {
        this.access = access;
        if (access >= 9) {
            this.canAdmin = true;
        } else {
            this.canAdmin = false;
        }
        if (access >= 8) {
            this.canEdit = true;
        } else {
            this.canEdit = false;
        }
    }

    public logOut() {

        const postData = {};
        postData['access_token'] = this.accessToken;
        const res = this.appService.postClient('sign/signout', postData);
        res.subscribe(() => { });
        this.signedIn = false;
        this.emailAddress = null;
        this.accessToken = null;
        this.access = 0;
        localStorage.removeItem('accessToken');
        this.navigateToPage('sign-in');
        // location.reload();
    }

    public navigateToPage(pagelink: string) {
        this.router.navigate([pagelink]);
    }
    public shuffleArray(array: any[]) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    public isValidMailFormat(value: string) {
        // tslint:disable-next-line: max-line-length
        const regexp: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (value.length > 4 && regexp.test(value)) {
            return true;
        }
        return false;
    }
    public isValidPassFormat(value: string) {
        if (value.length > 5) {
            return true;
        }
        return false;
    }
    public isValidZipCodeFormat(value: string) {
        value = value.trim();
        const regexp = new RegExp(/^\d{5}(-\d{4})?$/);
        if (value.length > 0 && regexp.test(value)) {
            return true;
        }
        return false;
    }
    public isValidNameFormat(value: string) {
        value = value.trim();
        const regexp = new RegExp(/^[a-zA-Z\s]+$/);
        if (value.length > 0 && regexp.test(value)) {
            return true;
        }
        return false;
    }
    public selectValueFromPrompid(arr: any[], checkKey: any, checkValue: any, getKey: any) {
        for (const entry of arr) {
            if (entry[checkKey] == checkValue) {
                return entry[getKey];
            }
        }
        return null;
    }
    public getItemIndexFromPrompt(arr: any[], checkKey: any, checkValue: any) {
        for (const entry of arr) {
            if (entry[checkKey] == checkValue) {
                return arr.indexOf(entry);
            }
        }
        return null;
    }
    public compareValues(key: any, order = 'asc', keyIsInt: boolean) {
        return function (a: any, b: any) {
            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0;
            }

            let varA = (typeof a[key] === 'string') ?
                a[key].toUpperCase() : a[key];

            let varB = (typeof b[key] === 'string') ?
                b[key].toUpperCase() : b[key];
            if (keyIsInt) {
                varA = parseInt(varA, 0);
                varB = parseInt(varB, 0);
            }

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }
    public scrollToButtom() {
        this.sleep(300).then(() => {
            window.scrollTo(0, document.body.scrollHeight);
        });

    }
    public scrollToButtomDiv(id: string) {
        this.sleep(300).then(() => {
            const objDiv = document.getElementById(id);
            objDiv.scrollTop = objDiv.scrollHeight;
        });

    }

    private sleep(time: number) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }
    public findMaxFromRecord(arr: any[], getKey: string): number {
        let max: number = null;
        for (const entry of arr) {
            if (max == null || parseInt(entry[getKey], 0) > max) {
                max = entry[getKey];
            }
        }
        return (max != null) ? max : 0;
    }
        public findMinFromRecord(arr: any[], getKey: string): number {
        let min: number = null;
        for (const entry of arr) {
            if (min == null || parseInt(entry[getKey], 0) < min) {
                min = entry[getKey];
            }
        }
        return (min != null) ? min : 0;
    }
}
